@extends('home')

@section('content')

	<div class="container">

		<!--Start show image of profiles in slide-->
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div id="carousel-id" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
						<div class="item">
							<img alt="First slide" src="../upload/categories/earth.png">
							<div class="container">
								<div class="carousel-caption">
									<h1>Example headline.</h1>
									<p>Note: If you're viewing this page via a <code>file://</code> URL, the "next" and "previous" Glyphicon buttons on the left and right might not load/display properly due to web browser security rules.</p>
									<p><a style="float: left" href="#">X</a></p>
									<p><a style="float: right" href="#"a></p>
								</div>
							</div>
						</div>
						<div class="item">
							<img data-src="holder.js/900x500/auto/#666:#6a6a6a/text:Second slide" alt="Second slide" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI5MDAiIGhlaWdodD0iNTAwIj48cmVjdCB3aWR0aD0iOTAwIiBoZWlnaHQ9IjUwMCIgZmlsbD0iIzY2NiI+PC9yZWN0Pjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9IjQ1MCIgeT0iMjUwIiBzdHlsZT0iZmlsbDojNmE2YTZhO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjU2cHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+U2Vjb25kIHNsaWRlPC90ZXh0Pjwvc3ZnPg==">
							<div class="container">
								<div class="carousel-caption">
									<h1>Another example headline.</h1>
									<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
									<p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
								</div>
							</div>
						</div>
						<div class="item active">
							<img data-src="holder.js/900x500/auto/#555:#5a5a5a/text:Third slide" alt="Third slide" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI5MDAiIGhlaWdodD0iNTAwIj48cmVjdCB3aWR0aD0iOTAwIiBoZWlnaHQ9IjUwMCIgZmlsbD0iIzU1NSI+PC9yZWN0Pjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9IjQ1MCIgeT0iMjUwIiBzdHlsZT0iZmlsbDojNWE1YTVhO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjU2cHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+VGhpcmQgc2xpZGU8L3RleHQ+PC9zdmc+">
							<div class="container">
								<div class="carousel-caption">
									<h1>One more for good measure.</h1>
									<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
									<p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
								</div>
							</div>
						</div>
					</div>
					<a class="left carousel-control" href="#carousel-id" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
					<a class="right carousel-control" href="#carousel-id" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
				</div>
			</div>
		</div>
		<!--End show image of profile in slide-->
			
		<!--Start show information of user-->
			<input type="text" value="{{ $profile->name}}">

			<?php
				$current_title=$profile->title;
				$title_array=['Ms.', 'Mr.', 'Mx.'];
				$title_position = array_search($current_title, $title_array);				
				
			?>
			<select name="title_list">
				@if($title_position!=false)
					<?php unset($title_array[$title_position]);?>
					<option value="{{$current_title}}">{{$current_title}}</option>
				@endif
				@foreach($title_array as $title)					
					<option value="{{$title}}">{{$title}}</option>
				@endforeach
			</select>

			<?php 
				$year_position = 0;
				for($year = 2001; $year >= 1920; $year--) {
					$year_array[$year_position] = $year;
					$year_position++;
				}
				$current_year=$profile->birth_year;

				$year_position=array_search($current_year, $year_array);
			?>

			<select name="year_list">
				@if($year_position!=false)
					<?php unset($year_array[$year_position]); ?>
					<option value="{{$current_year}}">{{$current_year}}</option>
				@endif
				@foreach($year_array as $year)					
					<option value="{{$year}}">{{$year}}</option>
				@endforeach
			</select>

			<?php 
				$unit_position=0;
				
				for($inch=60; $inch<=106; $inch++){
					$foot_unit=$inch/12;
					$inch_unit=$inch%12;
					$feet=$foot_unit."'".$inch_unit.'"';

					$unit_array[$unit_position]=$feet;
					$unit_position++;
				}
				var_dump($unit_array);
			?>

		<!--End show information of user-->

		<!--Start show rating-->

		<!--End show rating-->

		<!--Start show last review-->

		<!--End show last review-->

		<!--Start show social-->

		<!--End show social-->
	</div>

@endsection