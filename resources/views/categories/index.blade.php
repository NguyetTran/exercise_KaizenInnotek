@extends('home')
@section('content')

<div class="container">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		
	</div>
  	@foreach ($categories as $cate)
	  	@if($cate->parent_id== null)
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 no-padding main has-sub"> 
				<span>{{$cate->name}}</span>		
				<ul class="main-title">	
		@endif
			@foreach ($cate->parentCategory as $subCate)					
				<li class="sub-title">{{$subCate->name}}</li>
			@endforeach
		 @if($cate->parent_id== null)
			</ul>		
		</div>
		@endif
	@endforeach
</div>

@endsection