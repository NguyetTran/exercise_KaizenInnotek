<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Profile;

class Image extends Model {

	protected $table='images';

	protected $fillable=['user_id', 'image'];

	protected $timestamps=true;

	public function profile(){
		return $this->belongsTo('App\Profile', 'profile_id');
	}

}
