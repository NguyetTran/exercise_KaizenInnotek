<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

	protected $table='categories';

	protected $fillable=['id','name', 'parent_id', 'child_id'];

	public $timestamps= true;

	public function parentCategory () {
		return $this->hasMany('App\Category', 'parent_id');
	}

	public function childernCategory () {
		return $this->belongsTo('App\Category','parent_id');
	}


}
