<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;

use App\Image;

class Profile extends Model {

	protected $table='profiles';

	protected $fillable=['id','user_id', 'name', 'title', 'birth_year', 'height', 'weight', 'language'];

	public $timestamps= true;

	public function user(){

		return $this->belongsTo('App\User', 'user_id');
	}

	public function image(){
		return $this->hasMany('App\Image');
	}

}
