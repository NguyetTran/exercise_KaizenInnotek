=<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		// $this->call('UserTableSeeder');
		DB::table('users')->insert([
            'name' => str_random(10),
            'email' => str_random(10).'@gmail.com',
            'password' => bcrypt('secret'),
        ]);
        
    	for($i = 1; $i<=3; $i++){
    		DB::table('categories')->insert([
    			'name' => 'Title'. $i,
	        ]);	       
    	}
    	for($i = 1; $i<=3; $i++){
    	 	for($j=$i; $j<=$i+4; $j++){
	        	DB::table('categories')->insert([
    				'name' => 'Sub - Title'. $j,
    				'parent_id' => $i
	        	]);
	        }
        } 
	}

}
